<?php

namespace oop\SimpleFactory;

use oop\AbstractFactory\FurnitureFactory;

class ModernFurnitureFactory implements FurnitureFactory
{
    public function produceChairs()
    {
        return new Chair();
    }

    public function produceSofas()
    {
        return new Sofa();
    }
}