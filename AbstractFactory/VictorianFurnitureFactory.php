<?php

namespace oop\SimpleFactory;

use oop\AbstractFactory\FurnitureFactory;

class VictorianFurnitureFactory implements FurnitureFactory
{
    public function produceChairs()
    {
        return new Chair();
    }

    public function produceSofas()
    {
        return new Sofa();
    }
}

$factoryForKing = new VictorianFurnitureFactory();

$sofaForKing = $factoryForKing->produceSofas();
$sofaForKing->hasLegs();

$chairForKing = $factoryForKing->produceChairs();
$chairForKing->hasLegs();



$factoryForHipster = new ModernFurnitureFactory();

$sofaForHipster = $factoryForHipster->produceSofas();
$sofaForHipster->hasLegs();

$chairForHipster = $factoryForHipster->produceChairs();
$chairForHipster->hasLegs();