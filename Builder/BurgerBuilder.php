<?php


class BurgerBuilder
{
    protected $cheese = false;
    protected $pepperoni = false;
    protected $lettuce = false;
    protected $tomato = false;

    /**
     * @param bool $cheese
     * @return $this
     */
    public function setCheese(bool $cheese)
    {
        $this->cheese = $cheese;
        return $this;
    }

    /**
     * @param bool $pepperoni
     * @return $this
     */
    public function setPepperoni(bool $pepperoni)
    {
        $this->pepperoni = $pepperoni;
        return $this;
    }

    /**
     * @param bool $lettuce
     * @return $this
     */
    public function setLettuce(bool $lettuce)
    {
        $this->lettuce = $lettuce;
        return $this;
    }

    /**
     * @param bool $tomato
     * @return $this
     */
    public function setTomato(bool $tomato)
    {
        $this->tomato = $tomato;
        return $this;
    }

    /**
     * @return Burger
     */
    public function build(): Burger
    {
        return new Burger($this);
    }
}

$builder = new BurgerBuilder();

$builder->setCheese(true);
$builder->setLettuce(false);
$builder->setPepperoni(false);
$builder->setTomato(true);

$makeNewTastySuperYammyBurger = $builder->build();