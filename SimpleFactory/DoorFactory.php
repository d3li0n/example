<?php

namespace oop\SimpleFactory;

class DoorFactory
{
    public static function create($height, $width, $material): Door
    {
        if ('iron' === $material) {
            return new IronDoor($height, $width);
        }
        return new WoodenDoor($height, $width);
    }
}

$woodenDoor = DoorFactory::create(43, 2000, 'wood');

$woodenDoor->getHeight();