<?php


namespace oop\SimpleFactory;

class WoodenDoor implements Door
{
    public $height;
    public $width;

    public function __construct($height, $width)
    {
        $this->height = $height;
        $this->width = $width;
    }

    public function getHeight(): float
    {
        // TODO: Implement getHeight() method.
    }

    public function getWidth(): float
    {
        // TODO: Implement getWidth() method.
    }
}