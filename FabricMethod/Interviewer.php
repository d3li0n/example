<?php


namespace oop\FabricMethod;


interface Interviewer
{
    public function askQuestion();
}